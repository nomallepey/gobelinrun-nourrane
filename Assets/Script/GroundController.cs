using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GroundController : MonoBehaviour
{
    public GameObject coin;
    SpawnPathManager spawnPathManager;
    // Start is called before the first frame update
    void Start()
    {
        spawnPathManager = GameObject.FindObjectOfType<SpawnPathManager>();
        spawnRandomCoin(GetComponent<BoxCollider>());
    }
    // Update is called once per frame
    void Update()
    {
    }

    private void spawnRandomCoin(BoxCollider boxCollider)
    {
        var x = Random.Range(boxCollider.bounds.min.x + 0.2f, boxCollider.bounds.max.x - 0.2f);
        for (int i = 0; i < 5; i++)
        {
            var temp = Instantiate(coin, transform);
            temp.transform.position = new Vector3(
               x,
               1,
               Random.Range(boxCollider.bounds.min.z+2, boxCollider.bounds.max.z-2));
        }
       
    }
    private void OnTriggerExit(Collider other)
    {
        if (other.name == "Player")
        {
            spawnPathManager.SpawnPath();
            Destroy(gameObject, 2);
        }
       
    }

}
