using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class GameManager : MonoBehaviour
{

    [SerializeField] int score;
    public static int bestScore;
    public static GameManager inst;
    public Text scoreText;
    public PlayerController playerController;
    public float increaseSpeed = 0f;

    private void UpdateDisplayScore()
    {
        scoreText.text = "Score : " + score + "\nMeilleur Score : " + bestScore;
    }
    public void Awake()
    {
        inst = this;
        UpdateDisplayScore();
    }
    public void ReintialiseScore()
    {
        if (score > bestScore) bestScore = score;
        score = 0;
    }
    public void IncreaseScore()
    {
        score++;
        UpdateDisplayScore();
        increaseSpeed += 0.05f;
    }
    // Start is called before the first frame update
    void Start()
    {
       
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
