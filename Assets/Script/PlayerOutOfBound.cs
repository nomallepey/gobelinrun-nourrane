using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerOutOfBound : MonoBehaviour
{
    // Start is called before the first frame update
    private float Range = 4.9f;
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (transform.position.x < -Range)
            transform.position = new Vector3(-Range, transform.position.y, transform.position.z);
        if (transform.position.x > Range)
            transform.position = new Vector3(Range, transform.position.y, transform.position.z);
    }
}
