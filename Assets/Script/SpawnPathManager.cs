using UnityEngine;

public class SpawnPathManager : MonoBehaviour
{
    // Start is called before the first frame update
    public GameObject Ground;
    public GameObject GroundFirst;
    Vector3 nextSpawnPoint;
    private float spawnInterval = 15;

    public void SpawnPath(bool isFirst = false)
    {
        GameObject temp;
        if (isFirst) temp = Instantiate(GroundFirst, nextSpawnPoint, Quaternion.identity);
        else temp = Instantiate(Ground, nextSpawnPoint, Quaternion.identity);
        nextSpawnPoint = temp.transform.GetChild(1).transform.position;
    }

    private void Start()
    {
        SpawnPath(true);
        for (int i = 0; i < spawnInterval; i++)
        {
           SpawnPath();
        }
    }
}
