using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnTreeManager : MonoBehaviour
{
    // Start is called before the first frame update

    public GameObject[] TreePrefabs;
    public GameObject [] PositionPrefabs;

    // Start is called before the first frame update
    void Start()
    {
        // SpawnRandomTree();
        //spawnInterval = Random.Range(0, 100);
        //InvokeRepeating("SpawnRandomTree", startDelay, spawnInterval);

        SpawnRandomTree();
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    void SpawnRandomTree()
    {
        var treeIndex = Random.Range(0, TreePrefabs.Length);
        int SpawnObstaclePosIndex = Random.Range(0, 3);
        Transform t =  transform.GetChild(2).GetChild(SpawnObstaclePosIndex).transform;
        Instantiate(TreePrefabs[treeIndex], t.position, Quaternion.identity, transform);
        /*
        spawnPosX = Random.Range(-spawnRangeX, spawnRangeX);
        spawnPosZ = Random.Range(-spawnRangeZ, spawnRangeZ);
        var rotation = TreePrefabs[treeIndex].transform.rotation;

        Vector3 spawnPos = new Vector3(spawnPosX, 0, spawnPosZ);
        Instantiate(TreePrefabs[treeIndex], spawnPos, rotation);
        */
    }
}
