using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UIElements;

public class CoinController : MonoBehaviour
{
    private 
    // Start is called before the first frame update
    void Start()
    {
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.name == "Player")
        {
            Destroy(gameObject);
            GameManager.inst.IncreaseScore();
        }
        else if (other.tag == "Obstacle" || other.tag == "Coin"){
            Destroy(gameObject);
        }
           
    }

    // Update is called once per frame
    void Update()
    {
        transform.Rotate(new Vector3(0,90,0) * Time.deltaTime);
    }
}
