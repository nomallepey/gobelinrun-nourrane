using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnAnimals : MonoBehaviour
{
    // Start is called before the first frame update

    public GameObject[] AnimalsPrefabs;
    public GameObject[] PositionPrefabs;

    // Start is called before the first frame update
    void Start()
    {
        var DisplayAnimals = Random.Range(0,1);
        if (DisplayAnimals == 0)
            SpawnRandomAnimal();
    }

    // Update is called once per frame
    void Update()
    {

    }
    void SpawnRandomAnimal()
    {
        var animalIndex = Random.Range(0, AnimalsPrefabs.Length);
        int SpawnAnimalPosIndex = Random.Range(0, PositionPrefabs.Length);
        Transform t = transform.GetChild(4).GetChild(SpawnAnimalPosIndex).transform;
        string name = transform.GetChild(4).GetChild(SpawnAnimalPosIndex).gameObject.name;  
        if (name == "Left")
            Instantiate(AnimalsPrefabs[animalIndex], t.position, Quaternion.LookRotation(new Vector3(1, 0, 0)), transform);
        else Instantiate(AnimalsPrefabs[animalIndex], t.position, Quaternion.LookRotation(new Vector3(-1, 0, 0)), transform);
    }
}
