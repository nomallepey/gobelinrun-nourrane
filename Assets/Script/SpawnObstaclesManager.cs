using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnObstaclesManager : MonoBehaviour
{
    // Start is called before the first frame update

    public GameObject[] ObstaclesPrefabs;
    public GameObject [] PositionPrefabs;

    // Start is called before the first frame update
    void Start()
    {

        SpawnRandomObstacles();
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    void SpawnRandomObstacles()
    {
        var obstacleIndex = Random.Range(0, ObstaclesPrefabs.Length);
        int SpawnObstaclePosIndex = Random.Range(0, PositionPrefabs.Length);
        Transform t =  transform.GetChild(3).GetChild(SpawnObstaclePosIndex).transform;
        //Instantiate(ObstaclesPrefabs[treeIndex], t.position,Quaternion.LookRotation(new Vector3(1,0,0)) , transform);
        Instantiate(ObstaclesPrefabs[obstacleIndex], t.position,Quaternion.identity , transform);
    }
}
