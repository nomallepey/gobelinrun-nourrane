
using Unity.VisualScripting;
using UnityEditor;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PlayerController : MonoBehaviour
{
    // Start is called before the first frame update
    public bool alive = true;
    private float jumpForce = 5f;
    private bool isOnGround = true;
    public float horizontalInput;
    private float speed = 8f;
    private float horizontalSpeed = 5f;
    public Rigidbody body;
    private float horizontalMultiplier = 1.5f;
    private Animator animator;
    void Start()
    {
        animator = this.GetComponent<Animator>();
    }

    private void FixedUpdate()
    {
        var currentSpeed = speed + GameManager.inst.increaseSpeed;
        if (!alive)
            return;
        Vector3 forwardMove = transform.forward * currentSpeed * Time.deltaTime;
        Vector3 horizontalMove = transform.right * horizontalInput * horizontalSpeed * Time.fixedDeltaTime * horizontalMultiplier;
        body.MovePosition(body.position + forwardMove + horizontalMove);
    }

    public void Die()
    {
        alive = false;
        Invoke("Restart", 2);
    }

    private void Restart()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
        GameManager.inst.ReintialiseScore();
    }
    // Update is called once per frame
    void Update()
    {
        if (transform.position.z < -5)
            alive = false;

        horizontalInput = Input.GetAxis("Horizontal");
        // Gerer les sauts
        if (Input.GetKeyDown(KeyCode.Space) && isOnGround)
        {
            Jump();
        }
        else if (transform.position.y > -1 && transform.position.y < 0.50)
            animator.SetBool("isJumping", false);
    }
    void Jump()
    {  
        animator.SetBool("isJumping", true);
        body.AddForce(Vector3.up * jumpForce, ForceMode.Impulse);
        isOnGround = false;
    }
    private void OnCollisionEnter(Collision collision)
    {
        isOnGround = true;
    }

}
