using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimalController : MonoBehaviour
{
    float speed;
    private Transform player;
    Vector3 offset;
    private float seuilMagnitude;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        player = GameObject.FindWithTag("Player").transform;
        if (this.transform.name.Contains( "Taipan"))
        {
            seuilMagnitude = 10;
            speed = 3 + GameManager.inst.increaseSpeed; 
        }else if (this.transform.name.Contains("Gecko"))
        {
            seuilMagnitude = 7;
            speed = 5 + GameManager.inst.increaseSpeed; 
        }
        else if (this.transform.name.Contains("Muskrat"))
        {
            seuilMagnitude = 3;
            speed = 5 + GameManager.inst.increaseSpeed;
        }

        offset = transform.position - player.position;

        if (offset.magnitude < seuilMagnitude)
            transform.Translate(Vector3.forward * Time.deltaTime * speed);
        if (transform.position.z < player.position.z || transform.position.x < -5  || transform.position.x > 5)
                Destroy(gameObject, 2);
    }
}
